### NODE MARIADB SKELETON 

This is development environment (NOT FOR PRODUCTION). You should install [Docker](https://www.docker.com/) and docker compose

![alt text](https://gitlab.com/shakh.andrew/mongo-replicase/raw/master/src/public/images/model.png)

Test project which create a replica set in MongoDB. it is a group of mongod processes that maintain the same data set.
There is simple nodejs (express) webserver which demonstrates work with a replica set of MongoDB. Example link: http://localhost:3000

### Quick start:
  - run install script `$ bash ./install.sh`
  - the site will be accessible via the link [http://localhost:3000/](http://localhost:3000/)
___

There are extra commands:

**Start all containers**
```sh
$ docker-compose up --build -d
```

**Stop all containers**
```sh
$ docker-compose down
```

**Login into container**
```sh
$ docker exec -it <name> bash
```

**List all containers**
```sh
$ docker ps -a
```

**Remove all images**
```sh
$ docker rmi $(docker images -q)
```

### Database
All db files are stored on host machine (safe mode). So you import a dump only one time and the data will be saved even if you stop or remove container.

**Import Mongo dump**
```sh
$ docker exec -i mongo mongoimport --username root --password qwerty --authenticationDatabase admin --db dbName --collection collectionName < backup/fileName.json
```

There is MongoExpress container. URL for access to it: [http://localhost:8081](http://localhost:8081/)

**DB credentials:**
  - root / qwerty
  - user / user
