const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    "name": String,
    "price": String,
    "availableColors": Array,
    "weight": String,
    "inStock": Number
});
module.exports = mongoose.model('item', itemSchema);
