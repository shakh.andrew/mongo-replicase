var express = require('express');
var router = express.Router();
var itemModel = require('../models/item');

/* GET home page. */
router.get('/', function (req, res, next) {

  itemModel.find().then((items) => {

    console.log('ITEMS = ', items);
    res.render('index', {
      items: items,
      error: '',
      title: 'Success'
    });
  }).catch(err => {
    console.log('===============================');
    console.log(err);
    console.log('===============================');
    res.render('index', {
      items: [],
      error: err,
      title: 'Error'
    });
  });


});

module.exports = router;