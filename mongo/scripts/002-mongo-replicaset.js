db.getSiblingDB('admin').grantRolesToUser("root", ["clusterManager"]);
rs.add("mongo-secondary:27017");
rs.addArb("mongo-arbiter:27017");
db.createUser(
    {
        user: "user-test",
        pwd: "password-test",
        roles: [
            {
                role: "readWrite",
                db: "testdb"
            }
        ]
    }
);

db.getSiblingDB('testdb').init.insert({ some_key: "some_value" });