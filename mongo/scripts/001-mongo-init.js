db.getSiblingDB('admin').createUser(
    {
        user: "root",
        pwd: "qwerty", // or cleartext password
        roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
    }
);






//=======================================================================================
// use admin
// db.grantRolesToUser("root", [ "clusterManager" ])
//mongo --username root --password qwerty --authenticationDatabase admin
//mongo --username root --password qwerty --host rs0/mongo-primary,mongo-secondary,mongo-arbiter
//rs.status()
//rs.conf()
// 1 — Primary
// 2 — Secondary
// 7 — Arbiter

//db.auth("root", "qwerty");


//
//
// rs.initiate( {
//     _id : "rs0",
//     members: [
//         { _id: 0, host: "mongo-primary:27017" },
//         { _id: 1, host: "mongo-secondary:27017" },
//         { _id: 2, host: "mongo-arbiter:27017" }
//     ]
// })