#!/bin/bash
cp src/.env.example src/.env
npm --prefix ./src install
chmod 400 ./mongo/key
docker-compose up --build -d
docker ps -a
sleep 60 | echo Sleeping 60
docker exec -i mongo-primary mongo /docker-entrypoint-initdb.d/000-rs-init.js
sleep 30 | echo Sleeping 30
docker exec -i mongo-primary mongo /docker-entrypoint-initdb.d/001-mongo-init.js
docker exec -i mongo-primary mongo --username root --password qwerty --authenticationDatabase admin /docker-entrypoint-initdb.d/002-mongo-replicaset.js
docker exec -i mongo-primary mongo "mongodb://mongo-primary:27017,mongo-secondary:27017/testdb?replicaSet=rs0" --authenticationDatabase admin --username root --password qwerty --eval "rs.status()"
sleep 30 | echo Sleeping 30
docker exec -i mongo-primary mongoimport --host mongo-primary:27017 -d testdb -c items --file /home/backup/items.json --username root --password qwerty --authenticationDatabase admin
docker-compose restart
